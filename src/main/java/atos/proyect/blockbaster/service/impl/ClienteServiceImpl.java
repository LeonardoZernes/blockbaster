package atos.proyect.blockbaster.service.impl;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Cliente;
import atos.proyect.blockbaster.entity.ClienteEntity;
import atos.proyect.blockbaster.entity.RoleEntity;
import atos.proyect.blockbaster.exceptions.ClienteBadRequestException;
import atos.proyect.blockbaster.exceptions.ClienteNoContentException;
import atos.proyect.blockbaster.repositories.ClienteRepository;
import atos.proyect.blockbaster.service.ClienteService;
import atos.proyect.blockbaster.service.TransformService;

@Service
public class ClienteServiceImpl implements ClienteService {
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private TransformService transformService;
	@Autowired
	private BCryptPasswordEncoder bcrypt;
	
	Logger logger = LoggerFactory.logger(ClienteServiceImpl.class);
	
	@Override
	public void addCliente(Cliente cliente) throws Exception {
		if (clienteRepository.findByDocumentoID(cliente.getDocumentoID()) == null) {
			clienteRepository.save(transformService.clienteToEntity(cliente));
			logger.info("El cliente " + cliente.getNombre() + " se ha añadido correctamente.");
		} else {
			logger.error("ClienteBadRequestException: El cliente ya se encuentra en la base de datos.");
			throw new ClienteBadRequestException("El cliente ya se encuentra en la base de datos.");
		}
	}
	@Override
	public Cliente searchCliente(String documentoID) throws Exception{
		ClienteEntity cliente = clienteRepository.findByDocumentoID(documentoID);
		if (cliente == null) {
			logger.error("ClienteNoContentException: no se encuentra el cliente en la base de datos.");
			throw new ClienteNoContentException("No se ha encontrado el cliente solicitado.");
		}
		return transformService.clienteToDto(cliente);
	}
	@Override
	public void deleteCliente(String documentoID) throws Exception {
		if (searchCliente(documentoID) != null) {
			ClienteEntity cliente = clienteRepository.findByDocumentoID(documentoID);
			for(RoleEntity rol : cliente.getRoles()) {
				rol.getListaClientes().remove(cliente);
			}
			clienteRepository.delete(cliente);
			logger.info("Se ha borrado el cliente correctamente.");
		} else {
			logger.error("ClienteBadRequestException: El cliente no se ha podido encontrar.");
			throw new ClienteBadRequestException("El cliente no se ha podido encontrar.");
		}
	}
	@Override
	public List<Cliente> listaClientes() throws Exception{
		List<ClienteEntity> listaClienteEnt = clienteRepository.findAll();
		if (listaClienteEnt.isEmpty()) {
			logger.warn("ClienteNoContentException: lista de clientes está vacía.");
			throw new ClienteNoContentException("La lista de clientes está vacía.");
		}
		List<Cliente> listaClienteDto = new ArrayList<>();
		for(ClienteEntity cliente : listaClienteEnt) {
			listaClienteDto.add(transformService.clienteToDto(cliente));
		}
		return listaClienteDto;
	}
	@Override
	public void modifyCliente(String documentoID, Cliente cliente) throws Exception {
		ClienteEntity newCliente = clienteRepository.findByDocumentoID(documentoID);
		if (newCliente == null) {
			logger.error("ClienteBadRequest: No se ha podido encontrar el cliente especificado");
			throw new ClienteBadRequestException("No se ha podido encontrar el cliente especificado");
		}
		newCliente.setNombre(cliente.getNombre());
		newCliente.setCorreo(cliente.getCorreo());
		newCliente.setFechaNac(cliente.getFechaNac());
		newCliente.setPassword(bcrypt.encode(cliente.getPassword()));
		clienteRepository.save(newCliente);
		logger.info("Cliente actualizado con éxito");
	}
}
