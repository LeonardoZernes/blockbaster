package atos.proyect.blockbaster.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import atos.proyect.blockbaster.enums.EnumState;
import lombok.Data;

@Data
public class Pedido {
	private Long pedidoID;
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/London")
	private LocalDate fechCreate = LocalDate.now();
	@NotNull(message = "Se ha de especificar el estado del pedido: VENDIDO/ALQUILADO")
	private EnumState state;
	private Cliente cliente = new Cliente();
	private Tienda tienda = new Tienda();
	private Juego gameRef = new Juego();
}
