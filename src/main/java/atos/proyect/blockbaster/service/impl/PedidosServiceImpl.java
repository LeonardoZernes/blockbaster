package atos.proyect.blockbaster.service.impl;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Pedido;
import atos.proyect.blockbaster.entity.ClienteEntity;
import atos.proyect.blockbaster.entity.JuegoEntity;
import atos.proyect.blockbaster.entity.PedidoEntity;
import atos.proyect.blockbaster.entity.TiendaEntity;
import atos.proyect.blockbaster.enums.EnumState;
import atos.proyect.blockbaster.exceptions.PedidoBadRequestException;
import atos.proyect.blockbaster.exceptions.PedidoForbiddenException;
import atos.proyect.blockbaster.exceptions.PedidoNoContentException;
import atos.proyect.blockbaster.repositories.ClienteRepository;
import atos.proyect.blockbaster.repositories.JuegoRepository;
import atos.proyect.blockbaster.repositories.PedidoRepository;
import atos.proyect.blockbaster.repositories.TiendaRepository;
import atos.proyect.blockbaster.service.ClienteService;
import atos.proyect.blockbaster.service.PedidosService;
import atos.proyect.blockbaster.service.TransformService;

@Service
public class PedidosServiceImpl implements PedidosService{
	@Autowired
	private PedidoRepository pedidoRepository;
	@Autowired
	private TiendaRepository tiendaRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private JuegoRepository juegoRepository;
	@Autowired
	private TransformService transformService;
	@Autowired
	private ClienteService clienteService;
	
	Logger logger = LoggerFactory.logger(PedidosServiceImpl.class); 
	
	@Override
	@Transactional
	public void addPedido(Pedido pedidoOld) throws Exception{
		PedidoEntity pedido = new PedidoEntity();
		TiendaEntity tiendaEnt = tiendaRepository.findByTiendaID(pedidoOld.getTienda().getTiendaID());
		JuegoEntity juegoEnt = juegoRepository.findByJuegoID(pedidoOld.getGameRef().getJuegoRef());
		ClienteEntity clienteEnt = clienteRepository.findByDocumentoID(pedidoOld.getCliente().getDocumentoID());
		
		if (tiendaEnt == null) {
			logger.error("PedidoBadRequestException: No existe la tienda.");
			throw new PedidoBadRequestException("No existe la tienda.");
		}
		if (juegoEnt == null) {
			logger.error("PedidoBadRequestException: No existe el juego especificado");
			throw new PedidoBadRequestException("No existe el juego especificado.");
		}
		
		pedido.setJuego(juegoEnt);
		pedido.setCliente(clienteEnt);
		pedido.setTienda(tiendaEnt);
		pedido.setState(pedidoOld.getState().toString());
		
		// Comrpobación de que el usuario tiene edad > PEGI
//		if (pedido.getGameRef().getPegi().compareTo(
//				Period.between(
//							pedido.getCliente().getFechaNac(),
//							LocalDate.now()
//						).getYears()) > 0) {
//			logger.error("PedidoForbiddenException: El cliente tiene menos de la edad requerida para comprar el juego.");
//			throw new PedidoForbiddenException("El cliente tiene menos de la edad requerida para comprar el juego.");
//		} else {
			pedidoRepository.save(pedido);
			logger.info("El pedido se ha añadido correctamente");
//		}
	}
	
	@Override
	public Pedido searchPedido(Long stockRef) throws Exception {
		PedidoEntity pedido = pedidoRepository.findByPedidoID(stockRef);
		if (pedido == null) {
			logger.error("PedidoNoContentException: El pedido no se encuentra en la base de datos");
			throw new PedidoNoContentException("El pedido no se encuentra en la base de datos.");
		}
		return transformService.pedidoToDto(pedido);
	}
	
	@Override
	@Transactional
	public void deletePedido(Long stockRef) throws Exception{
		// WARN: si usamos la función searchPedido() nos da un error de deatached entity passed to persist
		PedidoEntity busqueda = pedidoRepository.findByPedidoID(stockRef);
		if (busqueda != null) {
			pedidoRepository.delete(busqueda);
			logger.info("Se ha borrado el pedido correctamente");
		} else {
			logger.error("PedidoBadRequestException: El pedido no se encuentra en la base de datos");
			throw new PedidoBadRequestException("El pedido especificado no existe en la base de datos.");
		}
	}
	
	@Override
	public List<Pedido> pedidosCliente(String documentoID) throws Exception{
		List<Pedido> listaPedidos = new ArrayList<Pedido>();
		ClienteEntity clienteBusqueda = clienteRepository.findByDocumentoID(documentoID);
		if (clienteBusqueda == null) {
			logger.error("PedidoNoContentException: El cliente especificado no se encuentra en la base de datos");
			throw new PedidoNoContentException("El cliente especificado no existe en la base de datos.");
		}
		if (clienteBusqueda.getPedidos().isEmpty()) {
			logger.error("PedidoNoContentException: El cliente no tiene pedidos asociados");
			throw new PedidoNoContentException("El cliente no tiene pedidos asociados");
		}
		for (PedidoEntity pedidoCliente : clienteBusqueda.getPedidos()) {
			listaPedidos.add(transformService.pedidoToDto(pedidoCliente));
		}
		return listaPedidos;
	}
}
