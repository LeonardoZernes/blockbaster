package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.BadRequestException;

public class RoleBadRequestException extends BadRequestException{

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Rol Bad Request";

	public RoleBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
	}

}
