package atos.proyect.blockbaster.service;

import java.util.List;

import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Company;

@Service
public interface CompanyService {
	public List<Company> listaCompanies();
	public void addCompany(Company company);
	public Company searchCompany(String cif);
	public void updateCompany(Company company);
	public void deleteCompany(String cif);
}
	
