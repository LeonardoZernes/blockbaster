package atos.proyect.blockbaster.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.entity.ClienteEntity;
import atos.proyect.blockbaster.entity.RoleEntity;
import atos.proyect.blockbaster.repositories.ClienteRepository;

// Se pone el transactional para que pueda coger las credenciales de rol correctamente
@Service
@Transactional
public class ClienteDetailsService implements UserDetailsService{
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		ClienteEntity cliente = clienteRepository.findByDocumentoID(username);
		
		List<GrantedAuthority> roles = new ArrayList<>();
		for(RoleEntity rol : cliente.getRoles()) {
			roles.add(new SimpleGrantedAuthority(rol.getRoleName()));
		}
//		roles.add(new SimpleGrantedAuthority("ADMIN"));
		
		UserDetails clienteDet = new User(cliente.getDocumentoID(), cliente.getPassword(), roles);
		return clienteDet;
	}

}
