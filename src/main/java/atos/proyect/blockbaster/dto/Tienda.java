package atos.proyect.blockbaster.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class Tienda {
	private Long tiendaID;
	@NotBlank(message = "El nombre de la tienda no puede estar vacío.")
	private String name;
	@NotBlank(message = "Se ha de especificar la dirección de la tienda.")
	private String address;
}
