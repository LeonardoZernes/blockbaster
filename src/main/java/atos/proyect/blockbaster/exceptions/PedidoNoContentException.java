package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.NoContentException;

public class PedidoNoContentException extends NoContentException{
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Pedido No Content";
	
	public PedidoNoContentException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
