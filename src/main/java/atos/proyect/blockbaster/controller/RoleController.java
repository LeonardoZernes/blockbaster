package atos.proyect.blockbaster.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import atos.proyect.blockbaster.dto.Role;
import atos.proyect.blockbaster.service.RoleService;

@RestController
@CrossOrigin
public class RoleController {
	@Autowired
	private RoleService roleService;
	// TODO testear que funciona (el delete con cascade, cuidado!!!)
	
	@PostMapping("/rol")
	public void addRol(@RequestBody Role role) {
		roleService.addRol(role);
	}
	
	@GetMapping("/listaRoles")
	public List<Role> listaRoles(){
		return roleService.listaRoles();
	}
	
	@DeleteMapping("/rol")
	public void deleteRol(@RequestParam String roleName) {
		roleService.deleteRol(roleName);
	}
}
