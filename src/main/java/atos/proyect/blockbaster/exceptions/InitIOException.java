package atos.proyect.blockbaster.exceptions;

import java.io.IOException;

public class InitIOException extends IOException{

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Init IO Exception";
	
	public InitIOException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
