package atos.proyect.blockbaster.service;

import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Cliente;
import atos.proyect.blockbaster.dto.Company;
import atos.proyect.blockbaster.dto.Juego;
import atos.proyect.blockbaster.dto.Pedido;
import atos.proyect.blockbaster.dto.Role;
import atos.proyect.blockbaster.dto.Tienda;
import atos.proyect.blockbaster.entity.ClienteEntity;
import atos.proyect.blockbaster.entity.CompanyEntity;
import atos.proyect.blockbaster.entity.JuegoEntity;
import atos.proyect.blockbaster.entity.PedidoEntity;
import atos.proyect.blockbaster.entity.RoleEntity;
import atos.proyect.blockbaster.entity.TiendaEntity;

@Service
public interface TransformService {
	public ClienteEntity clienteToEntity(Cliente cliente) throws Exception;
	public CompanyEntity companyToEntity(Company company);
	public TiendaEntity tiendaToEntity(Tienda tienda);
	public JuegoEntity juegoToEntity(Juego juego);
	public PedidoEntity pedidoToEntity(Pedido pedido);
	public RoleEntity roleToEntity(Role role);
	
	public Pedido pedidoToDto(PedidoEntity pedido);
	public Cliente clienteToDto(ClienteEntity cliente);
	public Tienda tiendaToDto(TiendaEntity tienda);
	public Juego juegoToDto(JuegoEntity juego);
	public Company companyToDto(CompanyEntity company);
	public Role roleToDto(RoleEntity role);
}
