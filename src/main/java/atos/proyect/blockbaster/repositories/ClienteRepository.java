package atos.proyect.blockbaster.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import atos.proyect.blockbaster.entity.ClienteEntity;

@Repository
public interface ClienteRepository extends JpaRepository<ClienteEntity, Long>{
	public ClienteEntity findByDocumentoID(String documentoID);
}
