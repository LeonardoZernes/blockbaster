package atos.proyect.blockbaster.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import atos.proyect.blockbaster.entity.PedidoEntity;

@Repository
public interface PedidoRepository extends JpaRepository<PedidoEntity, Long>{
	public PedidoEntity findByPedidoID(Long stockRef);
}
