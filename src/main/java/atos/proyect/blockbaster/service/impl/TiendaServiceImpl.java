package atos.proyect.blockbaster.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Tienda;
import atos.proyect.blockbaster.entity.TiendaEntity;
import atos.proyect.blockbaster.exceptions.GestionBadRequestException;
import atos.proyect.blockbaster.exceptions.GestionNoContentException;
import atos.proyect.blockbaster.repositories.TiendaRepository;
import atos.proyect.blockbaster.service.TiendaService;
import atos.proyect.blockbaster.service.TransformService;

@Service
public class TiendaServiceImpl implements TiendaService {
	@Autowired
	private TiendaRepository tiendaRepository;
	@Autowired
	private TransformService transformService;
	
	Logger  logger = LoggerFactory.logger(TiendaServiceImpl.class);

	@Override
	public List<Tienda> listaTiendas() {
		List<TiendaEntity> listaTiendaEnt = tiendaRepository.findAll();
		if (listaTiendaEnt.isEmpty()) {
			logger.warn("GestionNoContentException: lista de tiendas está vacía.");
			throw new GestionNoContentException("La lista de tiendas está vacía.");
		}
		List<Tienda> listaTiendaDto = new ArrayList<>();
		for(TiendaEntity tienda : listaTiendaEnt) {
			listaTiendaDto.add(transformService.tiendaToDto(tienda));
		}
		return listaTiendaDto;
	}

	@Override
	public void addTienda(Tienda tienda) {
		if (tiendaRepository.findByName(tienda.getName()) == null) {
			tiendaRepository.save(transformService.tiendaToEntity(tienda));
			logger.info("Se ha generado el registro de una nueva tienda " + tienda.getName());
		} else {
			logger.error("La tienda " + tienda.getName() + " ya existe.");
			throw new GestionBadRequestException("La tienda ya existe.");
		}
	}

	@Override
	public Tienda searchTienda(String name) {
		TiendaEntity tienda = tiendaRepository.findByName(name);
		if (tienda == null) {
			logger.error("GestionNoContentException: no se encuentra la tienda en la base de datos.");
			throw new GestionNoContentException("No se ha encontrado la tienda solicitado.");
		}
		return transformService.tiendaToDto(tienda);
	}

	@Override
	public void updateTienda(Tienda tienda) {
		TiendaEntity newTienda = tiendaRepository.findByTiendaID(tienda.getTiendaID());
		if (newTienda == null) {
			logger.error("GestionBadRequestException: No se ha podido encontrar la tienda especificado");
			throw new GestionBadRequestException("No se ha podido encontrar la tienda especificado");
		}
		newTienda.setName(tienda.getName());
		newTienda.setAddress(tienda.getAddress());
		tiendaRepository.save(newTienda);
		logger.info("Tienda actualizada con éxito");
		System.out.println("FIN DE UPDATE");
	}

	@Override
	public void deleteTienda(String tName) {
		if (tiendaRepository.findByName(tName) != null) {
			tiendaRepository.delete(tiendaRepository.findByName(tName));
			logger.info("Se ha borrado la tienda correctamente");
		} else {
			logger.error("No se ha encontrado la tienda especificada");
			throw new GestionBadRequestException("No se ha encontrado la tienda especficiada.");
		}
	}

}
