package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.BadRequestException;

public class InitBadRequestException extends BadRequestException{
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Init Bad Request";
			
	public InitBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
	}
	

}
