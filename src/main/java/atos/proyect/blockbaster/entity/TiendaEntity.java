package atos.proyect.blockbaster.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name="TIENDA")
@Data
public class TiendaEntity {
	@Id
	@Column(name="TIENDA_ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long tiendaID;
	@Column(name="NOMBRE", unique=true)
	private String name;
	@Column(name="DIRECCION")
	private String address;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "tienda")
	private List<PedidoEntity> pedidos = new ArrayList<>();
}
