package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.ForbiddenException;

public class PedidoForbiddenException extends ForbiddenException{
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Forbidden Exception (403)";
	
	public PedidoForbiddenException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
