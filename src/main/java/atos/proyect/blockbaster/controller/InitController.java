package atos.proyect.blockbaster.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import atos.proyect.blockbaster.service.InitService;

@RestController
@CrossOrigin
public class InitController {
	@Autowired
	private InitService initService;
	
	@PostMapping("/inicio")
	public void iniciando() throws Exception {
		initService.inicio();
	}
}
