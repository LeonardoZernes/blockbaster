package atos.proyect.blockbaster.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Privilege {
	private String priviName;
	private List<Role> roleList = new ArrayList<>();
}
