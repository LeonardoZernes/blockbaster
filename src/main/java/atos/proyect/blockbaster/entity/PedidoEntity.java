package atos.proyect.blockbaster.entity;

import java.time.LocalDate;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name="PEDIDO")
@SequenceGenerator(name="pedidoRef", initialValue=100000, allocationSize=1)
@Data
public class PedidoEntity {
	@Id
	@Column(name="PEDIDO_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pedidoRef")
	private Long pedidoID;
	@Column(name="FECH_CREADO")
	private LocalDate fechCreate = LocalDate.now();
	@Column(name="ESTADO")
	private String state;
	@ManyToOne
	private ClienteEntity cliente;
	@ManyToOne
	private TiendaEntity tienda;
	@ManyToOne
	private JuegoEntity juego;
	
}
