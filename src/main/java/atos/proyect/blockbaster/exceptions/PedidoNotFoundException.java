package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.NotFoundException;

public class PedidoNotFoundException extends NotFoundException{
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Pedido Not Found";
	
	public PedidoNotFoundException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
