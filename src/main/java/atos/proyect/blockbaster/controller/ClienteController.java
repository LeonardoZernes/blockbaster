package atos.proyect.blockbaster.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import atos.proyect.blockbaster.dto.Cliente;
import atos.proyect.blockbaster.service.ClienteService;

@RestController
@CrossOrigin(origins ="*")
public class ClienteController {
	@Autowired
	private ClienteService clienteService;
	
	@PostMapping("/cliente")
	public void addCliente(@Valid @RequestBody Cliente cliente) throws Exception {
		clienteService.addCliente(cliente);
	}
	
	@GetMapping("/cliente")
	public Cliente searchCliente(@RequestParam String documentoID) throws Exception {
		return clienteService.searchCliente(documentoID);
	}
	
	@GetMapping("/cliente/list")
	public List<Cliente> listaClientes() throws Exception {
		return clienteService.listaClientes();
	}
	
	@PutMapping("/cliente")
	public void modifyCliente(@RequestParam String documentoID, @Valid @RequestBody Cliente cliente) throws Exception {
		clienteService.modifyCliente(documentoID, cliente);
	}
	
	@DeleteMapping("/cliente")
	public void deleteCliente(@RequestParam String documentoID) throws Exception {
		clienteService.deleteCliente(documentoID);
	}
}
