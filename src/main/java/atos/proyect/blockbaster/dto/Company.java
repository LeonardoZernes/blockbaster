package atos.proyect.blockbaster.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class Company {
	@NotBlank(message = "Se ha de introducir un CIF para la desarrolladora.")
	private String cif;
	@NotBlank(message = "Es obligatorio establecer un nombre para la desarrolladora.")
	private String name;
}
