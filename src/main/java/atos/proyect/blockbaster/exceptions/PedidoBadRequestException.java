package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.BadRequestException;

public class PedidoBadRequestException extends BadRequestException {
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Pedido Bad Request";
	
	public PedidoBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
