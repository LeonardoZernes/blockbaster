package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.NoContentException;

public class ClienteNoContentException extends NoContentException{
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Cliente No Content";
	
	public ClienteNoContentException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
