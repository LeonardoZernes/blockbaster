package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.BadRequestException;

public class ClienteBadRequestException extends BadRequestException{
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Cliente Bad Request";
	
	public ClienteBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
