package atos.proyect.blockbaster.exceptions.handler;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import atos.proyect.blockbaster.dto.ErrorResponse;
import atos.proyect.blockbaster.exceptions.InitIOException;
import atos.proyect.blockbaster.exceptions.PedidoNoContentException;
import atos.proyect.blockbaster.exceptions.generics.BadRequestException;
import atos.proyect.blockbaster.exceptions.generics.ForbiddenException;
import atos.proyect.blockbaster.exceptions.generics.NoContentException;
import atos.proyect.blockbaster.exceptions.generics.NotFoundException;

@Order(Ordered.HIGHEST_PRECEDENCE) // Esto nos dice que este handler tiene la mayor prioridad
@ControllerAdvice
public class BlockBasterHandlerException extends ResponseEntityExceptionHandler{
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({
		org.springframework.dao.DuplicateKeyException.class,
		javax.validation.ConstraintViolationException.class,
		BadRequestException.class
	})
	@ResponseBody
	public ErrorResponse badRequestException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler({
		NoContentException.class
		// TODO preguntar por no content como manejar error desde angular si 200?
	})
	@ResponseBody
	public ErrorResponse noContentException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler({
		ForbiddenException.class
	})
	@ResponseBody
	public ErrorResponse forbiddenException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({
		InitIOException.class
	})
	@ResponseBody
	public ErrorResponse initIOException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({
		NotFoundException.class
	})
	@ResponseBody
	public ErrorResponse notFoundException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	//Para que no salte mensaje de error en el frontEND
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ExceptionHandler({
		PedidoNoContentException.class
	})
	@ResponseBody
	public ErrorResponse pedidoNoContentException(HttpServletRequest request, Exception exception) {
		return new ErrorResponse(exception, request.getRequestURI());
	}
	
	// Campo para controlar las validaciones y que se muestren con el mismo formato de ResponseEntity
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request){
		List<String> errorMessages = ex.getBindingResult().getFieldErrors().stream().map( e -> e.getDefaultMessage()).collect(Collectors.toList());
		
		//return new ResponseEntity<>(new ErrorResponse(ex, errorMessages.toString()), status);
		//Se ha cambiado a como está abajo para que muestre los errores de las validaciones de forma correcta
		return new ResponseEntity<Object>(new ErrorResponse(ex, errorMessages.toString(), ((ServletWebRequest)request).getRequest().getRequestURI()), status); 
	}
	
}
