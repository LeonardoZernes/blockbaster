package atos.proyect.blockbaster.service;

import java.util.List;

import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Role;

@Service
public interface RoleService {
	public void addRol(Role role);
	public void deleteRol(String roleName);
	public List<Role> listaRoles();
}
