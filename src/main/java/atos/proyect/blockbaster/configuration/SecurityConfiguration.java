package atos.proyect.blockbaster.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import atos.proyect.blockbaster.service.ClienteDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private ClienteDetailsService clienteDetailsService;

//	@Autowired
//	private BCryptPasswordEncoder bcrypt;
	
	@Bean
	public BCryptPasswordEncoder passEncoder() {
		return new BCryptPasswordEncoder();
	}
	
    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(clienteDetailsService).passwordEncoder(passEncoder());
//        auth.inMemoryAuthentication()
//	        .withUser("admin")
//			.password(passEncoder().encode("admin"))
//			.roles("USER", "ADMIN"); // TODO No funciona inMemoryAuth mezclado ywy
        //Para autentificación de usuario en memoria
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	// Activar los POST y los DELETE que vienen por defecto en off y decirle que todas
    	// las consultas deben estar correctamente autentificadas.
      http.csrf().disable();
      http.authorizeRequests()
//      	.antMatchers("/cliente").hasAuthority("ADMIN")
//      	.antMatchers("/cliente/list").permitAll() //hasAuthority("ADMIN") -- se ofrece acceso
//      	.antMatchers("/manag_juego").hasAuthority("ADMIN")
//      	.antMatchers("/manag_tienda").hasAuthority("ADMIN")
//      	.antMatchers("/manag_company").hasAuthority("ADMIN")
//      	.antMatchers("/inicio").hasAuthority("ADMIN")
//      	.antMatchers("/rol").hasAuthority("ADMIN")
//      	.antMatchers("/listaRoles").hasAuthority("ADMIN")
//      	.antMatchers(HttpMethod.DELETE, "/pedido").hasAuthority("ADMIN")
      	.anyRequest().permitAll().and().httpBasic();
      //http.csrf().disable().httpBasic().and().authorizeRequests().anyRequest().authenticated();
    }
}
