package atos.proyect.blockbaster.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name="JUEGO")
@SequenceGenerator(name="juegoRef", initialValue=1000, allocationSize=50)
@Data
public class JuegoEntity {
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long juegoID;
	@Column(name="TITULO", unique=true)
	private String title;
	@Column(name="PEGI")
	private Integer pegi;
	@Column(name="FECHA_LANZ")
	private LocalDate launchDate;
	@Column(name="CATEGORY")
	private String category;
	@ManyToMany(mappedBy = "juegos")
	private List<CompanyEntity> companies = new ArrayList<CompanyEntity>();
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "juego")
	private List<PedidoEntity> pedidos = new ArrayList<>();
}
