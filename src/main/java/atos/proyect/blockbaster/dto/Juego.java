package atos.proyect.blockbaster.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import atos.proyect.blockbaster.enums.EnumCategory;
import lombok.Data;

@Data
public class Juego {
	private Long juegoRef;
	@NotBlank(message = "Se debe establecer el título del juego.")
	private String title;
	@NotNull(message = "Se debe introducir la fecha de lanzamiento.")
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/London")
	private LocalDate launchDate;
	private EnumCategory category;
	@NotBlank(message = "Se ha de especificar la clasificación PEGI.")
	private Integer pegi;
	private List<Company> companyName = new ArrayList<Company>();
}
