package atos.proyect.blockbaster.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import atos.proyect.blockbaster.dto.Juego;
import atos.proyect.blockbaster.service.JuegoService;

@RestController
@CrossOrigin
public class JuegoController {
	@Autowired
	private JuegoService juegoService;
	
	@GetMapping("/juego/list")
	public List<Juego> listaJuegos(){
		return juegoService.listaJuegos();
	}
	
	@GetMapping("/juego")
	public Juego getJuego(@RequestParam Long id) throws Exception {
		return juegoService.searchJuego(id);
	}
	
	@PostMapping("/juego")
	public void addJuego(@RequestBody Juego juego) {
		juegoService.addJuego(juego);
	}
	
	@PutMapping("/juego")
	public void updateJuego(@RequestBody Juego juego) {
		juegoService.updateJuego(juego);
	}
	
	@DeleteMapping("/juego")
	public void deleteJuego(@RequestParam Long id) {
		juegoService.deleteJuego(id);
	}
	
	
}
