package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.NoContentException;

public class GestionNoContentException extends NoContentException{
	
	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Gestion No Content";
	
	public GestionNoContentException(String details) {
		super(DESCRIPTION + ". " + details);
	}
}
