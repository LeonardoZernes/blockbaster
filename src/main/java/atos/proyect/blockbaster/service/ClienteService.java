package atos.proyect.blockbaster.service;
import java.util.List;

import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Cliente;

@Service
public interface ClienteService {
	public void addCliente(Cliente cliente) throws Exception;
	public Cliente searchCliente(String documentoID) throws Exception;
	public void deleteCliente(String documentoID) throws Exception;
	public List<Cliente> listaClientes() throws Exception;
	public void modifyCliente(String documentoID, Cliente cliente) throws Exception;
}
