package atos.proyect.blockbaster.service.impl;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.List;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Cliente;
import atos.proyect.blockbaster.dto.Company;
import atos.proyect.blockbaster.dto.Juego;
import atos.proyect.blockbaster.dto.Role;
import atos.proyect.blockbaster.dto.Tienda;
import atos.proyect.blockbaster.enums.EnumCategory;
import atos.proyect.blockbaster.enums.EnumRol;
import atos.proyect.blockbaster.exceptions.ClienteBadRequestException;
import atos.proyect.blockbaster.exceptions.GestionBadRequestException;
import atos.proyect.blockbaster.exceptions.InitBadRequestException;
import atos.proyect.blockbaster.exceptions.InitIOException;
import atos.proyect.blockbaster.repositories.ClienteRepository;
import atos.proyect.blockbaster.repositories.CompanyRepository;
import atos.proyect.blockbaster.repositories.JuegoRepository;
import atos.proyect.blockbaster.repositories.RoleRepository;
import atos.proyect.blockbaster.repositories.TiendaRepository;
import atos.proyect.blockbaster.service.ClienteService;
import atos.proyect.blockbaster.service.CompanyService;
import atos.proyect.blockbaster.service.InitService;
import atos.proyect.blockbaster.service.JuegoService;
import atos.proyect.blockbaster.service.RoleService;
import atos.proyect.blockbaster.service.TiendaService;
import atos.proyect.blockbaster.service.TransformService;

@Service
public class InitServiceImpl implements InitService {
	@Autowired
	private ClienteService clienteService;
	@Autowired
	private JuegoService juegoService;
	@Autowired
	private TiendaService tiendaService;
	@Autowired
	private CompanyService companyService;
	@Autowired
	private TransformService transformService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private JuegoRepository juegoRepository;
	@Autowired
	private TiendaRepository tiendaRepositoy;
	@Autowired
	private RoleRepository roleRepository;
	
	private static final String PATH = System.getProperty("user.dir") + "\\json\\";
	private Boolean serviceFirstTime = true;
	
	Logger logger = LoggerFactory.logger(InitServiceImpl.class); 
	
	@Override
	public void inicio() throws Exception {
		if (serviceFirstTime) {
			lecturaTiendas();
			lecturaCompanys();
			// Juegos depués de desarrolladoras para agregar referencias
			lecturaJuegos();
			lecturaRoles();
			// Cliente después de roles para agregar autentificación
			lecturaClientes();
			
			serviceFirstTime = false;
			logger.info("Cargna inicial de datos en la base de datos completada");
		} else {
			logger.error("Operación abortada: la carga inicial ya ha sido ejecutada");
			throw new InitBadRequestException("Operación abortada: la carga inicial ya ha sido ejecutada");
		}
	}
	@SuppressWarnings("unchecked")
	private void lecturaClientes() throws Exception {
		JSONParser parser = new JSONParser();

        try {
            // Introducimos una lista de JSONs
            JSONArray jsonList = (JSONArray) parser.parse(new FileReader(PATH + "clientes.json"));
            
			Iterator<JSONObject> iterator = jsonList.iterator();
			while (iterator.hasNext()) {
				Cliente nuevoCliente = new Cliente();
				JSONObject elementoActual = iterator.next();
				nuevoCliente.setDocumentoID((String) elementoActual.get("documentoID"));
				
				// Comprobamos que si ya está el cliente para que no salte exception de duplicado
				if (clienteRepository.findByDocumentoID(nuevoCliente.getDocumentoID())==null) {
					nuevoCliente.setNombre((String) elementoActual.get("nombre"));
					nuevoCliente.setCorreo((String) elementoActual.get("correo"));
					nuevoCliente.setPassword((String) elementoActual.get("password"));
					
					// Establecer Roles
		            if (elementoActual.get("roles") == null) {
		            	nuevoCliente.getRoles().add(EnumRol.valueOf("USER"));
		            } else if (((List<String>) elementoActual.get("roles")).isEmpty()){
		            	nuevoCliente.getRoles().add(EnumRol.valueOf("USER"));
		            } else {
		            	nuevoCliente.setRoles((List<EnumRol>) elementoActual.get("roles"));
		            }
		            
		            String fechaNac = (String) elementoActual.get("fechaNac").toString();
		            nuevoCliente.setFechaNac(LocalDate.parse(fechaNac, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		            
		            clienteService.addCliente(nuevoCliente);
				} else {
					logger.warn("Entrada no añadida. Cliente ya en la base de datos.");
				}
			}

        } catch (IOException e) {
        	logger.error(e.toString() + " No se ha encontrado el archivo JSON para clientes.");
        	throw new InitIOException("No se ha encontrado el archivo JSON para clientes.");
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        	logger.error(e.toString() + " El JSON no tiene descritos los campos correctamente para cliente.");
        	throw new ClienteBadRequestException("El JSON no tiene descritos los campos correctamente para cliente.");
        }
     }
	// Lectura inicial para crear las compañías de videojuegos.
	@SuppressWarnings("unchecked")
	private void lecturaCompanys() throws Exception {
		JSONParser parser = new JSONParser();
		try {
			JSONArray jsonList = (JSONArray) parser.parse(new FileReader(PATH + "companys.json"));
			Iterator<JSONObject> iterator = jsonList.iterator();
			while (iterator.hasNext()) {
				Company nuevaCompany = new Company();
				JSONObject elementoActual = iterator.next();
				nuevaCompany.setCif((String) elementoActual.get("cif"));
				nuevaCompany.setName((String) elementoActual.get("name"));
				
				// Comprobamos para que no salte exception
	            if (companyRepository.findByCif(nuevaCompany.getCif())==null) {
		            companyService.addCompany(nuevaCompany);
	            } else {
	            	logger.warn("Entrada no añadida. Company ya en la base de datos.");
				}
			}
		} catch (IOException e) {
			logger.error(e.toString() + " No se ha encontrado el archivo JSON para company.");
			throw new InitIOException("No se ha encontrado el archivo JSON para company.");
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        	logger.error(e.toString() + " El JSON no tiene descritos los campos correctamente para company.");
        	throw new GestionBadRequestException("El JSON no tiene descritos los campos correctamente para company.");
        }
	}
	// Lectura inicial para crear la lista de videojuegos disponibles.
	@SuppressWarnings("unchecked")
	private void lecturaJuegos() throws Exception {
		JSONParser parser = new JSONParser();
		try {
			JSONArray jsonList = (JSONArray) parser.parse(new FileReader(PATH + "juegos.json"));
			Iterator<JSONObject> iterator = jsonList.iterator();
			while (iterator.hasNext()) {
				Juego nuevoJuego = new Juego();
				JSONObject elementoActual = iterator.next();
				nuevoJuego.setTitle((String) elementoActual.get("title"));
				
				// Comprobamos si está el juego para no saltar exception
	            if (juegoRepository.findByTitle(nuevoJuego.getTitle())==null) {
					String launchDate = (String) elementoActual.get("launchDate").toString();
		            nuevoJuego.setLaunchDate(LocalDate.parse(launchDate, DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		            
		            //nuevoJuego.setCompanyName((List<String>) elementoActual.get("company"));
		            for (String companyName : (List<String>) elementoActual.get("company")) {
		            	nuevoJuego.getCompanyName().add(transformService.companyToDto(companyRepository.findByName(companyName)));
		            }
		            nuevoJuego.setCategory((EnumCategory) EnumCategory.valueOf(elementoActual.get("category").toString()));
		            // Pegi lo paso a Integer porque desde JSON lo pilla como Long
		            nuevoJuego.setPegi(Math.toIntExact((Long) elementoActual.get("pegi")));
		            
		            juegoService.addJuego(nuevoJuego);
	            } else {
	            	logger.warn("Entrada no añadida. Juego ya en la base de datos.");
				}
			}
		} catch (IOException e) {
			logger.error(e.toString() + " No se ha encontrado el archivo JSON para juegos.");
            throw new InitIOException("No se ha encontrado el archivo JSON para juegos.");
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        	logger.error(e.toString() + " El JSON no tiene descritos los campos correctamente para juego.");
        	throw new GestionBadRequestException("El JSON no tiene descritos los campos correctamente para juego.");
        }
	}
	// Lectura inicial para crear Tiendas desde cero.
	@SuppressWarnings("unchecked")
	private void lecturaTiendas() throws Exception {
		JSONParser parser = new JSONParser();
		try {
			JSONArray jsonList = (JSONArray) parser.parse(new FileReader(PATH + "tiendas.json"));
			Iterator<JSONObject> iterator = jsonList.iterator();
			while (iterator.hasNext()) {
				Tienda nuevaTienda = new Tienda();
				JSONObject elementoActual = iterator.next();
				nuevaTienda.setName((String) elementoActual.get("name"));
				
				// Comprobamos si hay tienda para no saltar exception
				if (tiendaRepositoy.findByName(nuevaTienda.getName())==null) {
					nuevaTienda.setAddress((String) elementoActual.get("address"));
					tiendaService.addTienda(nuevaTienda);
				} else {
					logger.warn("Entrada no añadida. Tienda ya en la base de datos.");
				}
			}
		} catch (IOException e) {
			logger.error(e.toString() + " No se ha encontrado el archivo JSON para tiendas.");
			throw new InitIOException("No se ha encontrado el archivo JSON para tiendas.");
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        	logger.error(e.toString() + " El JSON no tiene descritos los campos correctamente para tienda.");
        	throw new GestionBadRequestException("El JSON no tiene descritos los campos correctamente para tienda.");
        }
	}
	// Lectura inicial de los Roles para autentificación
	@SuppressWarnings("unchecked")
	private void lecturaRoles() throws Exception {
		JSONParser parser = new JSONParser();
		try {
			JSONArray jsonList = (JSONArray) parser.parse(new FileReader(PATH + "roles.json"));
			Iterator<JSONObject> iterator = jsonList.iterator();
			while (iterator.hasNext()) {
				Role nuevoRol = new Role();
				JSONObject elementoActual = iterator.next();
				nuevoRol.setRoleName(EnumRol.valueOf((String) elementoActual.get("roleName")));
				
				// Comprobamos que el rol no esté en la BBDD
				if (roleRepository.findByRoleName(nuevoRol.getRoleName().toString()) == null){
					roleService.addRol(nuevoRol);
				} else {
					logger.warn("Entrada no añadida. Rol ya en la base de datos.");
				}
			}
		} catch (IOException e) {
			logger.error(e.toString() + " No se ha encontrado el archivo JSON para roles.");
			throw new InitIOException("No se ha encontrado el archivo JSON para roles.");
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
        	logger.error(e.toString() + " El JSON no tiene descritos los campos correctamente para roles.");
        	throw new GestionBadRequestException("El JSON no tiene descritos los campos correctamente para roles.");
        }
	}
}
