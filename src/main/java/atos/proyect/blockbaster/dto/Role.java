package atos.proyect.blockbaster.dto;

import java.util.ArrayList;
import java.util.List;

import atos.proyect.blockbaster.enums.EnumRol;
import lombok.Data;

@Data
public class Role {
	private EnumRol roleName;
	private List<Cliente> userList = new ArrayList<>();
}
