package atos.proyect.blockbaster.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Role;
import atos.proyect.blockbaster.entity.RoleEntity;
import atos.proyect.blockbaster.exceptions.RoleBadRequestException;
import atos.proyect.blockbaster.exceptions.RoleNoContentException;
import atos.proyect.blockbaster.repositories.RoleRepository;
import atos.proyect.blockbaster.service.RoleService;
import atos.proyect.blockbaster.service.TransformService;

@Service
public class RoleServiceImpl implements RoleService{
	@Autowired
	private TransformService transformService;
	@Autowired
	private RoleRepository roleRepository;
	
	Logger logger = LoggerFactory.logger(RoleServiceImpl.class); 
	
	@Override
	public void addRol(Role role) {
		if (roleRepository.findByRoleName(role.getRoleName().toString()) == null) {
			roleRepository.save(transformService.roleToEntity(role));
			logger.info("Se ha añadido el rol: " + role.getRoleName().toString());
		} else {
			logger.error("RoleBadRequestException: El rol ya se encuentra en la base de datos");
			throw new RoleBadRequestException("El rol ya se encuentra en la base de datos.");
		}
	}
	
	@Override
	public List<Role> listaRoles(){
		List<Role> listaRoles = new ArrayList<>();
		for(RoleEntity role : roleRepository.findAll()) {
			listaRoles.add(transformService.roleToDto(role));
		}
		if (listaRoles.isEmpty()) {
			logger.warn("Lista de roles vacía");
			throw new RoleNoContentException("Lista de roles vacía");
		}
		return listaRoles;
	}

	@Override
	public void deleteRol(String roleName) {
		if (roleRepository.findByRoleName(roleName) != null) {
			roleRepository.delete(roleRepository.findByRoleName(roleName));
			logger.info("Se ha borrado el rol: " + roleName);
		} else {
			logger.error("El rol no se encuentra en la base de datos");
			throw new RoleBadRequestException("El rol no se encuentra en la base de datos.");
		}
	}
	
}
