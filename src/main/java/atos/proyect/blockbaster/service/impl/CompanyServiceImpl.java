package atos.proyect.blockbaster.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Company;
import atos.proyect.blockbaster.entity.CompanyEntity;
import atos.proyect.blockbaster.entity.JuegoEntity;
import atos.proyect.blockbaster.exceptions.GestionBadRequestException;
import atos.proyect.blockbaster.exceptions.GestionNoContentException;
import atos.proyect.blockbaster.repositories.CompanyRepository;
import atos.proyect.blockbaster.service.CompanyService;
import atos.proyect.blockbaster.service.TransformService;

@Service
public class CompanyServiceImpl implements CompanyService {
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private TransformService transformService;
	
	Logger logger  = LoggerFactory.logger(CompanyServiceImpl.class);
	// TODO: cambiar todas las excepciones
	@Override
	public List<Company> listaCompanies() {
		List<CompanyEntity> listaCompanyEnt = companyRepository.findAll();
		if (listaCompanyEnt.isEmpty()) {
			logger.warn("GestionNoContentException: lista de companies está vacía.");
			throw new GestionNoContentException("La lista de companies está vacía.");
		}
		List<Company> listaCompanyDto = new ArrayList<>();
		for(CompanyEntity company : listaCompanyEnt) {
			listaCompanyDto.add(transformService.companyToDto(company));
		}
		return listaCompanyDto;
	}

	@Override
	public void addCompany(Company company) {
		if (companyRepository.findByCif(company.getCif()) == null) {
			companyRepository.save(transformService.companyToEntity(company));
			logger.info("Se ha añadido la desarrolladora: " + company.getName());
		} else {
			logger.error("La company " + company.getName() + " ya existe.");
			throw new GestionBadRequestException("La company ya existe.");
		}
	}

	@Override
	public Company searchCompany(String cif) {
		CompanyEntity company = companyRepository.findByCif(cif);
		if (company == null) {
			logger.error("GestionNoContentException: no se encuentra la company en la base de datos.");
			throw new GestionNoContentException("No se ha encontrado la company solicitado.");
		}
		return transformService.companyToDto(company);
	}

	@Override
	public void updateCompany(Company company) {
		CompanyEntity newCompany = companyRepository.findByCif(company.getCif());
		if (newCompany == null) {
			logger.error("GestionBadRequestException: No se ha podido encontrar el cliente especificado.");
			throw new GestionBadRequestException("No se ha podido encontrar el cliente especificado.");
		}
		newCompany.setName(company.getName());
		companyRepository.save(newCompany);
		logger.info("Company actualizada con éxito");
		
	}

	@Override
	public void deleteCompany(String cif) {
		CompanyEntity company = companyRepository.findByCif(cif);
		if (company != null) {
			for (JuegoEntity j : company.getJuegos()) {
				j.getCompanies().remove(company);
			}
			companyRepository.delete(company);
			logger.info("Se ha borrado la company correctamente");
		} else {
			logger.error("No se ha encontrado la company especificada");
			throw new GestionBadRequestException("No se ha encontrado la company especficiada.");
		}
	}
	
}
