package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.BadRequestException;

public class GestionBadRequestException extends BadRequestException{

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Gestion Bad Request";

	public GestionBadRequestException(String details) {
		super(DESCRIPTION + ". " + details);
	}

}
