package atos.proyect.blockbaster.repositories;

import org.springframework.stereotype.Repository;

import atos.proyect.blockbaster.entity.RoleEntity;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long>{
	public RoleEntity findByRoleName(String name);
	public List<RoleEntity> findAll();
}
