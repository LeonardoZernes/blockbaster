package atos.proyect.blockbaster.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import atos.proyect.blockbaster.entity.TiendaEntity;

@Repository
public interface TiendaRepository extends JpaRepository<TiendaEntity, Long>{
	public TiendaEntity findByTiendaID(Long tiendaID);
	public TiendaEntity findByName(String name);
}
