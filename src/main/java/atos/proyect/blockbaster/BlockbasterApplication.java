package atos.proyect.blockbaster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlockbasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlockbasterApplication.class, args);
	}

}
