package atos.proyect.blockbaster.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import atos.proyect.blockbaster.dto.Tienda;
import atos.proyect.blockbaster.service.TiendaService;

@RestController
@CrossOrigin
public class TiendaController {
	@Autowired
	private TiendaService tiendaService;
	
	@GetMapping("/tienda/list")
	public List<Tienda> listaTiendas(){
		return tiendaService.listaTiendas();
	}
	
	@GetMapping("/tienda")
	public Tienda getTienda(@RequestParam String name) throws Exception {
		return tiendaService.searchTienda(name);
	}
	
	@PostMapping("/tienda")
	public void addTienda(@RequestBody Tienda tienda) {
		tiendaService.addTienda(tienda);
	}
	
	@PutMapping("/tienda")
	public void updateTienda(@RequestBody Tienda tienda) {
		tiendaService.updateTienda(tienda);
	}
	
	@DeleteMapping("/tienda")
	public void deleteTienda(@RequestParam String name) {
		tiendaService.deleteTienda(name);
	}
}
