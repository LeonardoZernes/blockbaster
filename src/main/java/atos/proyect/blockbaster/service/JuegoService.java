package atos.proyect.blockbaster.service;

import java.util.List;

import org.springframework.stereotype.Service;
import atos.proyect.blockbaster.dto.Juego;

@Service
public interface JuegoService {
	public List<Juego> listaJuegos();
	public void addJuego(Juego juego);
	public Juego searchJuego(Long id);
	public void updateJuego(Juego juego);
	public void deleteJuego(Long id);	
}
