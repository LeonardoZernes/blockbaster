package atos.proyect.blockbaster.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Company;
import atos.proyect.blockbaster.dto.Juego;
import atos.proyect.blockbaster.entity.CompanyEntity;
import atos.proyect.blockbaster.entity.JuegoEntity;
import atos.proyect.blockbaster.exceptions.GestionBadRequestException;
import atos.proyect.blockbaster.exceptions.GestionNoContentException;
import atos.proyect.blockbaster.repositories.CompanyRepository;
import atos.proyect.blockbaster.repositories.JuegoRepository;
import atos.proyect.blockbaster.service.JuegoService;
import atos.proyect.blockbaster.service.TransformService;

@Service
public class JuegoServiceImpl implements JuegoService{
	@Autowired
	private JuegoRepository juegoRepository;
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private TransformService transformService;
	
	Logger logger = LoggerFactory.logger(JuegoServiceImpl.class);
	
	@Override
	public List<Juego> listaJuegos() {
		List<JuegoEntity> listaJuegoEnt = juegoRepository.findAll();
		if (listaJuegoEnt.isEmpty()) {
			logger.warn("GestionNoContentException: lista de juegos está vacía.");
			throw new GestionNoContentException("La lista de juegos está vacía.");
		}
		List<Juego> listaJuegoDto = new ArrayList<>();
		for(JuegoEntity juego : listaJuegoEnt) {
			listaJuegoDto.add(transformService.juegoToDto(juego));
		}
		return listaJuegoDto;
	}
	
	@Override
	public void addJuego(Juego juego) {
		if (juegoRepository.findByJuegoID(juego.getJuegoRef()) == null) {
			juegoRepository.save(transformService.juegoToEntity(juego));
			logger.info("Se ha añadido el registro para el juego " + juego.getTitle());
		} else {
			logger.error("La juego " + juego.getTitle() + " ya existe.");
			throw new GestionBadRequestException("El juego ya existe.");
		}
	}
	
	@Override
	public Juego searchJuego(Long id) {
		JuegoEntity juego = juegoRepository.findByJuegoID(id);
		if (juego == null) {
			logger.error("GestionNoContentException: no se encuentra el juego en la base de datos.");
			throw new GestionNoContentException("No se ha encontrado el juego solicitado.");
		}
		return transformService.juegoToDto(juego);
	}
	
	@Override
	public void updateJuego(Juego juego) {
		JuegoEntity newJuego = juegoRepository.findByJuegoID(juego.getJuegoRef());
		if (newJuego == null) {
			logger.error("GestionBadRequestException: No se ha podido encontrar el juego especificado");
			throw new GestionBadRequestException("No se ha podido encontrar el juego especificado");
		}
		newJuego.setPegi(juego.getPegi());
		newJuego.setCategory(juego.getCategory().toString());
		newJuego.setLaunchDate(juego.getLaunchDate());
		for (CompanyEntity company : newJuego.getCompanies()) {
			company.getJuegos().remove(newJuego);
			// Esta línea deja la ultima compañía sin borrar :v
			//newJuego.getCompanies().remove(company);
		}
		for (Company company : juego.getCompanyName()) {
			CompanyEntity c = companyRepository.findByCif(company.getCif());
			newJuego.getCompanies().add(c);
			c.getJuegos().add(newJuego);
		}
		juegoRepository.save(newJuego);
	}
	
	@Override
	public void deleteJuego(Long id) {
		JuegoEntity juego = juegoRepository.findByJuegoID(id);
		if (juego != null) {
			for(CompanyEntity c : juego.getCompanies()) {
				c.getJuegos().remove(juego);
			}
			juegoRepository.delete(juego);
			logger.info("Se ha borrado la juego correctamente");
		} else {
			logger.error("No se ha encontrado la juego especificada");
			throw new GestionBadRequestException("No se ha encontrado el juego especficiado.");
		}
	}
}
