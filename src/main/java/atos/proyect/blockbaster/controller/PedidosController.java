package atos.proyect.blockbaster.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import atos.proyect.blockbaster.dto.Pedido;
import atos.proyect.blockbaster.service.PedidosService;

@RestController
@CrossOrigin
public class PedidosController {
	@Autowired
	private PedidosService pedidosService;
	
	@PostMapping("/pedido")
	public void addPedido(@RequestBody Pedido pedido) throws Exception {
		pedidosService.addPedido(pedido);
	}
	
	@GetMapping("/pedido")
	public Pedido searchPedido(@RequestParam Long id) throws Exception {
		return pedidosService.searchPedido(id);
	}
	
	@DeleteMapping("/pedido")
	public void deletePedido(@RequestParam Long id) throws Exception{
		pedidosService.deletePedido(id);
	}
	
	@GetMapping("/pedido/cliente")
	public List<Pedido> pedidosCliente(@RequestParam String documentoID) throws Exception{
		return pedidosService.pedidosCliente(documentoID);
	}
}
