package atos.proyect.blockbaster.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "CLIENTE")
@Data
public class ClienteEntity {  
	@Id
	@Column(name = "DOCUMENT")
	private String documentoID;
	@Column(name = "PASSWORD")
	private String password;
	@Column(name = "NAME")
	private String nombre;
	@Column(name = "CORREO")
	private String correo;
	@Column(name = "FECHA_NAC")
	private LocalDate fechaNac;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
	private List<PedidoEntity> pedidos = new ArrayList<>();
	@ManyToMany(cascade = CascadeType.MERGE, mappedBy = "listaClientes")
	private List<RoleEntity> roles = new ArrayList<>();
}
