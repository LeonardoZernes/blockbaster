package atos.proyect.blockbaster.service.impl;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Cliente;
import atos.proyect.blockbaster.dto.Company;
import atos.proyect.blockbaster.dto.Juego;
import atos.proyect.blockbaster.dto.Pedido;
import atos.proyect.blockbaster.dto.Role;
import atos.proyect.blockbaster.dto.Tienda;
import atos.proyect.blockbaster.entity.ClienteEntity;
import atos.proyect.blockbaster.entity.CompanyEntity;
import atos.proyect.blockbaster.entity.JuegoEntity;
import atos.proyect.blockbaster.entity.PedidoEntity;
import atos.proyect.blockbaster.entity.RoleEntity;
import atos.proyect.blockbaster.entity.TiendaEntity;
import atos.proyect.blockbaster.enums.EnumCategory;
import atos.proyect.blockbaster.enums.EnumRol;
import atos.proyect.blockbaster.enums.EnumState;
import atos.proyect.blockbaster.exceptions.RoleBadRequestException;
import atos.proyect.blockbaster.repositories.ClienteRepository;
import atos.proyect.blockbaster.repositories.CompanyRepository;
import atos.proyect.blockbaster.repositories.JuegoRepository;
import atos.proyect.blockbaster.repositories.RoleRepository;
import atos.proyect.blockbaster.repositories.TiendaRepository;
import atos.proyect.blockbaster.service.TransformService;

@Service
public class TransformServiceImpl implements TransformService{
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private TiendaRepository tiendaRepository;
	@Autowired
	private JuegoRepository juegoRepository;
	@Autowired
	private RoleRepository rolesRepository;
	@Autowired
	private BCryptPasswordEncoder bcrypt;
	
	Logger logger = LoggerFactory.logger(TransformServiceImpl.class); 
	
	@Override
	public ClienteEntity clienteToEntity(Cliente cliente) throws Exception{
		ClienteEntity clienteEnt = new ClienteEntity();
		clienteEnt.setNombre(cliente.getNombre());
		clienteEnt.setCorreo(cliente.getCorreo());
		clienteEnt.setDocumentoID(cliente.getDocumentoID());
		clienteEnt.setFechaNac(cliente.getFechaNac());
		clienteEnt.setPassword(bcrypt.encode(cliente.getPassword()));
		if (cliente.getRoles().isEmpty()) {
			RoleEntity newRol = rolesRepository.findByRoleName("USER");
			if (newRol == null) {
				logger.error("RoleBadRequestException: El rol USER no se encuentra en la lista de roles");
				throw new RoleBadRequestException("El rol USER no se encuentra en la lista de roles");
			}
			logger.warn("Default role: USER");
			clienteEnt.getRoles().add(newRol);
			newRol.getListaClientes().add(clienteEnt);
		} else {
			for(EnumRol rol : cliente.getRoles()) {
				RoleEntity newRol = rolesRepository.findByRoleName(rol.toString());
				if (newRol == null) {
					logger.error("RoleBadRequestException: El rol especificado en el JSON no se encuentra en la lista de roles");
					throw new RoleBadRequestException("El rol especificado en el JSON no se encuentra en la lista de roles");
				}
				clienteEnt.getRoles().add(newRol);
				newRol.getListaClientes().add(clienteEnt);
			}
		}
		return clienteEnt;
	}
	@Override
	public Cliente clienteToDto(ClienteEntity cliente) {
		Cliente clienteDto = new Cliente();
		clienteDto.setDocumentoID(cliente.getDocumentoID());
		clienteDto.setCorreo(cliente.getCorreo());
		clienteDto.setFechaNac(cliente.getFechaNac());
		clienteDto.setNombre(cliente.getNombre());
		// TODO algo para conseguir la contraseña anterior
		clienteDto.setPassword(null);
		for(RoleEntity rol : cliente.getRoles()) {
			clienteDto.getRoles().add(EnumRol.valueOf(rol.getRoleName()));
		}
		return clienteDto;
	}
	@Override
	public CompanyEntity companyToEntity(Company company) {
		CompanyEntity companyEnt = new CompanyEntity();
		companyEnt.setName(company.getName());
		companyEnt.setCif(company.getCif());
		return companyEnt;
	}
	@Override
	public Company companyToDto(CompanyEntity company) {
		Company companyDto = new Company();
		companyDto.setCif(company.getCif());
		companyDto.setName(company.getName());
		return companyDto;
	}
	@Override
	public TiendaEntity tiendaToEntity(Tienda tienda) {
		TiendaEntity tiendaEnt = new TiendaEntity();
		tiendaEnt.setAddress(tienda.getAddress());
		tiendaEnt.setName(tienda.getName());
		return tiendaEnt;
	}
	@Override
	public Tienda tiendaToDto(TiendaEntity tienda) {
		Tienda tiendaDto = new Tienda();
		tiendaDto.setTiendaID(tienda.getTiendaID());
		tiendaDto.setName(tienda.getName());
		tiendaDto.setAddress(tienda.getAddress());
		return tiendaDto;
	}
	@Override
	public JuegoEntity juegoToEntity(Juego juego) {
		JuegoEntity juegoEnt = new JuegoEntity();
		juegoEnt.setPegi(juego.getPegi());
		juegoEnt.setCategory(juego.getCategory().toString());
		juegoEnt.setLaunchDate(juego.getLaunchDate());
		juegoEnt.setTitle(juego.getTitle());
		for (Company companyName : juego.getCompanyName()) {
			CompanyEntity c = companyRepository.findByCif(companyName.getCif());
			juegoEnt.getCompanies().add(c);
			c.getJuegos().add(juegoEnt);
		}
		return juegoEnt;
	}
	@Override
	public Juego juegoToDto(JuegoEntity juego) {
		Juego juegoDto = new Juego();
		// WARN: no está pasado la lista de pedidos del juego entity
		juegoDto.setJuegoRef(juego.getJuegoID());
		juegoDto.setTitle(juego.getTitle());
		juegoDto.setPegi(juego.getPegi());
		juegoDto.setLaunchDate(juego.getLaunchDate());
		juegoDto.setCategory(EnumCategory.valueOf(juego.getCategory()));
		for (CompanyEntity company : juego.getCompanies()) {
			juegoDto.getCompanyName().add(companyToDto(company));
		}
		return juegoDto;
	}
	@Override
	public PedidoEntity pedidoToEntity(Pedido pedido) {
		PedidoEntity pedidoEnt = new PedidoEntity();
		pedidoEnt.setPedidoID(pedido.getPedidoID());
		pedidoEnt.setFechCreate(pedido.getFechCreate());
		pedidoEnt.setState(pedido.getState().toString());
		
		ClienteEntity clienteAsociado = clienteRepository.findByDocumentoID(pedido.getCliente().getDocumentoID());
		pedidoEnt.setCliente(clienteAsociado);
		clienteAsociado.getPedidos().add(pedidoEnt);
		TiendaEntity tiendaAsociada = tiendaRepository.findByName(pedido.getTienda().getName());
		pedidoEnt.setTienda(tiendaAsociada);
		tiendaAsociada.getPedidos().add(pedidoEnt);
		JuegoEntity juegoAsociado = juegoRepository.findByJuegoID(pedido.getGameRef().getJuegoRef());
		pedidoEnt.setJuego(juegoAsociado);
		juegoAsociado.getPedidos().add(pedidoEnt);
		
		return pedidoEnt;
	}
	@Override
	public Pedido pedidoToDto(PedidoEntity pedido) {
		Pedido pedidoDto = new Pedido();
		pedidoDto.setPedidoID(pedido.getPedidoID());
		pedidoDto.setFechCreate(pedido.getFechCreate());
		pedidoDto.setState(EnumState.valueOf(pedido.getState()));
		pedidoDto.setGameRef(juegoToDto(pedido.getJuego()));
		pedidoDto.setCliente(clienteToDto(pedido.getCliente()));
		pedidoDto.setTienda(tiendaToDto(pedido.getTienda()));
		return pedidoDto;
	}
	@Override
	public RoleEntity roleToEntity(Role role) {
		RoleEntity newRol = new RoleEntity();
		newRol.setRoleName(role.getRoleName().toString());
		return newRol;
	}
	@Override
	public Role roleToDto(RoleEntity role) {
		Role newRol = new Role();
		newRol.setRoleName(EnumRol.valueOf(role.getRoleName()));
		for(ClienteEntity c : role.getListaClientes()) {
			newRol.getUserList().add(clienteToDto(c));
		}
		return newRol;
	}
}
