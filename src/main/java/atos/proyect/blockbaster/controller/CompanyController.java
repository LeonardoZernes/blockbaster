package atos.proyect.blockbaster.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import atos.proyect.blockbaster.dto.Company;
import atos.proyect.blockbaster.service.CompanyService;

@RestController
@CrossOrigin
public class CompanyController {
	@Autowired
	private CompanyService companyService;
	
	@GetMapping("/company/list")
	public List<Company> listaCompanies() {
		return companyService.listaCompanies();
	}
	
	@GetMapping("/company")
	public Company getCompany(@RequestParam String cif) {
		return companyService.searchCompany(cif);
	}
	
	@PostMapping("/company")
	public void addCompany(@RequestBody Company company) {
		companyService.addCompany(company);
	}
	
	@PutMapping("/company")
	public void updateCompany(@RequestBody Company company) {
		companyService.updateCompany(company);
	}
	
	@DeleteMapping("/company")
	public void deleteCompany(@RequestParam String cif) {
		companyService.deleteCompany(cif);
	}
}
