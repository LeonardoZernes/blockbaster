package atos.proyect.blockbaster.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import atos.proyect.blockbaster.enums.EnumRol;
import lombok.Data;

@Data // Se encarga de crear los getters y los setters de forma automatica.
public class Cliente {
	@NotBlank(message="El nombre no puede quedar en blanco")
	@Size(min=2, max=20, message="Nombre fuera de rango: debe ser al menos de 2 caracteres y menos de 20.")
	private String nombre;
	
	@NotBlank(message ="El email no puede quedar en blanco")
	@Email(message="El formato de email no está correcto.")
	private String correo;
	
	@NotBlank(message="Se debe introducir un documento identificativo.")
	@Pattern(regexp = "[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]", message="El formato del documento no corresponde a NIF/NIE.")
	private String documentoID;
	
	@NotBlank(message="La password no puede quedar en blanco")
	@Size(min=8, max=15, message="La password debe tener al menos 8 caractéres y como máximo 15")
	private String password;
	
	@NotNull(message="Debe introducir una fecha de nacimiento válida.")
	//@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/London")
	private LocalDate fechaNac;
	
	private List<EnumRol> roles = new ArrayList<>();
}
