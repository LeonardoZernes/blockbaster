package atos.proyect.blockbaster.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "ROL")
@Data
public class RoleEntity {
  
    @Id
    @Column(name="ROL_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long roleID;
    
    @Column(name = "ROL_NAME", unique = true)
    private String roleName;
    
    @ManyToMany(cascade = CascadeType.ALL)
    private List<ClienteEntity> listaClientes = new ArrayList<>();
}
