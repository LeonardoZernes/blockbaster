package atos.proyect.blockbaster.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "COMPANY")
@Data
public class CompanyEntity {
	@Column(name="NOMBRE", unique=true)
	private String name;
	@Id
	@Column(name="CIF")
	private String cif;
	
	@ManyToMany(cascade = CascadeType.ALL) // Interesa más eliminar los juegos cuando se borra una company
	private List<JuegoEntity> juegos = new ArrayList<>();
}
