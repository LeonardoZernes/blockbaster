package atos.proyect.blockbaster.service;

import org.springframework.stereotype.Service;

@Service
public interface InitService {
	public void inicio() throws Exception;
}
