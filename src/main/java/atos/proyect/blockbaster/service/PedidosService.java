package atos.proyect.blockbaster.service;

import java.util.List;

import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Pedido;

@Service
public interface PedidosService {
	public void addPedido(Pedido pedido) throws Exception;
	public Pedido searchPedido(Long stockRef) throws Exception;
	public List<Pedido> pedidosCliente(String documentoID) throws Exception;
	public void deletePedido(Long stockRef) throws Exception;
}
