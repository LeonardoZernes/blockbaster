package atos.proyect.blockbaster.exceptions;

import atos.proyect.blockbaster.exceptions.generics.NoContentException;

public class RoleNoContentException extends NoContentException {

	private static final long serialVersionUID = 1L;
	private static final String DESCRIPTION = "Role No Content Exception";

	public RoleNoContentException(String details) {
		super(DESCRIPTION + ". " + details);
	}

}
