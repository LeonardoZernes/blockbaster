package atos.proyect.blockbaster.service;

import java.util.List;

import org.springframework.stereotype.Service;

import atos.proyect.blockbaster.dto.Tienda;

@Service
public interface TiendaService {
	public List<Tienda> listaTiendas();
	public void addTienda(Tienda tienda);
	public Tienda searchTienda(String name);
	public void updateTienda(Tienda tienda);
	public void deleteTienda(String tName);
}
