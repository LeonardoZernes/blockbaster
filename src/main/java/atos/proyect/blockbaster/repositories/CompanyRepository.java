package atos.proyect.blockbaster.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import atos.proyect.blockbaster.entity.CompanyEntity;

@Repository
public interface CompanyRepository extends JpaRepository<CompanyEntity, String>{
	public CompanyEntity findByCif(String cif);
	public CompanyEntity findByName(String name);
}
