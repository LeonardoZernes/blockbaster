package atos.proyect.blockbaster.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import atos.proyect.blockbaster.entity.JuegoEntity;

public interface JuegoRepository extends JpaRepository<JuegoEntity, Long>{
	public JuegoEntity findByTitle(String title);
	public JuegoEntity findByJuegoID(Long juegoID);
}
